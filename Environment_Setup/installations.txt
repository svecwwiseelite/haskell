How to install
For most Un*x operating systems, the easiest way to install is to run:

curl -sSL https://get.haskellstack.org/ | sh
or:
wget -qO- https://get.haskellstack.org/ | sh

On Windows, you can download and install the Windows 64-bit Installer.
For other operating systems and direct downloads, check out the install and upgrade guide.

Note that the get.haskellstack.org script will ask for root access using sudo in order to use your platform's package manager to install dependencies and to install to /usr/local/bin. If you prefer more control, follow the manual installation instructions in the install and upgrade guide.

How to upgrade
If you already have stack installed, upgrade it to the latest version by running:
stack upgrade

Quick Start Guide
First you need to install it (see previous section).

Start your new project:
stack new my-project
cd my-project
stack setup
stack build
stack exec my-project-exe

The stack new command will create a new directory containing all the needed files to start a project correctly.
The stack setup will download the compiler if necessary in an isolated location (default ~/.stack) that won't interfere with any system-level installations. (For information on installation paths, please use the stack path command.).
The stack build command will build the minimal project.
stack exec my-project-exe will execute the command.

If you just want to install an executable using stack, then all you have to do is stack install <package-name>.
If you want to launch a REPL:
stack ghci
Run stack for a complete list of commands.

