import Data.List

pos :: Int -> Int -> Int -> Int
pos n k t | (5 * t * (t + 1)) > (2 * k) && t <= n = t - 1
                | otherwise = pos n k (t + 1)

main = do
print $ pos  4 (240 - 190) 1

