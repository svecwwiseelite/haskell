import Data.List

removeDuplicates :: (Eq a) => [a] -> [a]
removeDuplicates list = remDups list []

remDups ::(Eq a) => [a] -> [a] -> [a]
remDups [] _ = []
remDups (x:xs) list2
    | (x `elem` list2) = remDups xs list2
    | otherwise = x : remDups xs (x:list2)

horseShoeCount :: [Int] -> Int
horseShoeCount l = 4 - length (removeDuplicates l)

main = do
     print $ horseShoeCount [1, 7, 3, 3]


