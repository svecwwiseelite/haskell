factorial :: Int -> Int
factorial n | n > 1 = n * factorial (n - 1)
            | otherwise = 1

isFactovisor :: Int -> Int -> Bool
isFactovisor a b = mod a b == 0

main = do
        a <- readLn
        b <- readLn
        if isFactovisor (factorial a) b
                then putStrLn $ show b ++ " divides " ++ show a ++ "!"
        else
                putStrLn $ show b ++ " does not divide " ++ show a ++ "!"

