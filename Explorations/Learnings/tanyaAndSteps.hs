import Data.List
import Data.Char

diff x = show $ zipWith (-) (tail x) x
 
getnum s = words s

digi n = map digitToInt $ show n

digits n = read  (map intToDigit n) :: Int

getNum s num = digits (digi (getOneIndices s num))

getOneIndices s num = diff $ elemIndices ("1") (getnum s) ++ num

main = do
	n <- getLine
	let num = (read $ n) :: Int
	s <- getLine
	print $ getNum s [num] 
