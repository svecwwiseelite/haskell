fizzBuzz :: Int -> String
fizzBuzz num | rem num 15 == 0 = "FizzBuzz"
             | rem num 3 == 0  = "Fizz"
             | rem num 5 == 0  = "Buzz"
             | otherwise = ""

generateSeries :: Int -> [String]
generateSeries ns = [if fizzBuzz n  == "" 
                      then show n 
                     else fizzBuzz n | n <- [1..ns]] 

main = do
       print $ generateSeries 10

