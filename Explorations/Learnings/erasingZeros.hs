import Data.List
import Control.Monad

slice x y s = take y (drop x s)
get1indices s = elemIndices ('1') s

firstindx s  = head (get1indices s)
endindx s = last (get1indices s)

substr s = slice (firstindx s) (endindx s) s

eraseZeros s = length $ elemIndices ('0') (substr s)

main = do
	t <-readLn
	forM_ [1..t] $ \t_itr -> do
 		s <- getLine
		print $ eraseZeros s

