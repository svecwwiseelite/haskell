import Data.List
import Data.Char(toLower)

isVowel :: Char -> Bool
isVowel ch = elem (toLower ch) "aeiou"

vowelCount :: String -> Int
vowelCount word = length $ filter isVowel word

wordsVowelCount :: [String] -> [Int]
wordsVowelCount words = [vowelCount word | word <- words]
wordsConsCount :: [String] -> [Int]
wordsConsCount words = [length (word) - vowelCount word | word <- words]


main = do
       print $ concat $ map show $ wordsVowelCount $ words "parrot egg not green"
