import Data.List
import Control.Break
import Prelude hiding (break)
import Control.Monad

match :: String -> String -> Int
match randNumber gNum = length $ filter id $ zipWith (==) randNumber gNum

main = do
	let t = 7
	forM_ [1..t] $ \t_itr -> do
	num <- getLine
	let guessNum = read num :: Int
	let randNumber = "1729"
	putStrLn $ "match : " ++ show(match randNumber num)
	let randNum = sort randNumber
	let givenNum = sort num
	putStrLn $ "exist : " ++ show(match randNum givenNum) 
	if length (randNumber) == (match randNumber num) then break()
