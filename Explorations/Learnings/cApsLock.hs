import Data.Char
case1 :: String -> Bool
case1 str =  all(True == )(map isUpper str)

strhead :: String -> Char
strhead str =  head str

strTail :: String -> String
strTail str = tail str

case2 :: String -> Bool
case2 str =  isLower (strhead str) && map (isUpper (strTail str))


main = do
       print $ case1 "reethu" || case2 "reethu"
                                                                          
