import Data.List
import Data.Char

isVowel s = elem s ['a', 'e', 'i', 'o', 'u']

vowelCount s = length $ filter isVowel s
consonantCount s = length $ filter (== False) [isVowel x | x <- s]

vowelList s = [show (vowelCount x + (10 * consonantCount x)) | x <- s]

--stringList s = [show x | x <- s]

main = do
        let str = "parrot egg not green"
        let strList = words str
        print $ vowelList strList



