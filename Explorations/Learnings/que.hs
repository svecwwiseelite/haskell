import Data.List
import Data.String

listToString :: [Int] -> [Char]
listToString queue = [if even q then 'e' else 'o' | q<- queue]  


canDistribute :: String -> Bool
canDistribute s = even $ length $ filter( == 'o') s

countIdli :: String -> [Int]
countIdli s | length ( s )== 0 = [0]
countIdli s | take 2 s == "oe" = [2] ++  countIdli ("o" ++ drop 2 s)
countIdli s | take 2 s == "oo" = [2] ++ countIdli ( drop 2 s)
countIdli s | otherwise = countIdli (dropWhile (== 'e') s)



main = do
  print $ listToString [2, 3, 4, 8, 10, 7, 12, 6, 9, 11]
  if canDistribute $ listToString [2, 3, 4, 8, 10, 7, 12, 6, 9, 11]
  then print $ sum $ countIdli $ listToString [2, 3, 4, 8, 10, 7, 12, 6, 9, 11]
  else print $ "Not Possible"
 
