module Data.Numbers.Primes (

  primes, wheelSieve
) where
primes :: Int -> [Int]
primes n = wheelSieve n

wheelSieve :: Int -> [Int]
wheelSieve k = reverse ps ++ map head (sieve p (cycle ns))
 where (p:ps,ns) = wheel k

main = do
      print $ primes 6

