import Data.Char
import Data.List

numToDigits  = (map digitToInt) . show

digitsToNum  = read.(map intToDigit)

smallest = digitsToNum.sort.numToDigits

largest = digitsToNum.reverse.sort.numToDigits

nextKaprekar n = largest n - smallest n

kaprekarSeq = iterate nextKaprekar

takeUntilRepeat [] = []
takeUntilRepeat [x] = [x]
takeUntilRepeat (x:xs) = if x == head xs then [x] else x : takeUntilRepeat xs

main = do
       print $ takeUntilRepeat $ kaprekarSeq 1234
