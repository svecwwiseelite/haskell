**Introduction:**

Haskell is a general-purpose, statically-typed, purely functional programming language with type inference and lazy evaluation.

**Features:**

* Statically Typed
* Purely Functional
* Lazy
* Wide Range of Packages

**Pre-requisites:**

* Ensure that curl is installed.

**Installation:**

* curl -sSL https://get.haskellstack.org/ | sh

**To check the version:**

* ghc --version
